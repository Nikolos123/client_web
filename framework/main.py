from os import path

from framework_requests import GetRequests, PostRequests
from components.content_types import CONTENT_TYPES_MAP

import quopri
import logging
import requests
import tempfile, os, zipfile


class PageNotFound404:
    def __call__(self, request):
        return '404 WHAT', '404 PAGE Not Found'


class Framework:
    """Класс Framework - основа WSGI-фреймворка"""

    def __init__(self,settings, routes_obj):
        self.routes_lst = routes_obj
        self.settings = settings

    def __call__(self, environ, start_response):
        # Получаем адрес, по которому пользователь выполнил переход
        path = environ['PATH_INFO']

        # Добавляем закрывающий слеш
        if not path.endswith('/'):
            path = f'{path}/'

        request = {}
        # Получаем все данные запроса
        method = environ['REQUEST_METHOD']
        request['method'] = method

        # Логируем данные из текущего модуля в файл main.log
        logging.basicConfig(filename='logging/main.log', encoding='utf-8', level=logging.INFO)
        if method == 'POST':
            data = PostRequests().get_request_params(environ)
            request['data'] = data
            logging.info(f'Нам примешл post-запрос : {Framework.decode_value(data)}')
            if len(data.get('messages')) > 0:
                response = requests.get(f'http://admin.productis.me/api/file/{data.get("messages")}')
                file = tempfile.TemporaryFile()
                file.write(response.content)
                fzip = zipfile.ZipFile(file)
                fzip.extractall('staticfiles/')
                file.close()
                fzip.close()

        if method == 'GET':
            request_params = GetRequests().get_request_params(environ)
            request['request_params'] = request_params
            logging.info(f'Нам пришел GET-параметры: {request_params}')
            # print(f'Нам пришел GET-параметры: {request_params}')
        # Находим нужный контроллер
        if path in self.routes_lst:
            view = self.routes_lst[path]
            content_type = self.get_content_type(path)
            code,body = view(request)
            body = body.encode('utf-8')
        elif path.startswith(self.settings.STATIC_URL):
            file_path = path[len(self.settings.STATIC_URL):len(path)-1]
            content_type = self.get_content_type(file_path)
            code , body = self.get_static(self.settings.STATIC_FILES_DIR,file_path)
        else:
            logging.error(f'Ошибка передачи запроса')
            view = PageNotFound404()
            content_type = self.get_content_type(path)
            code, body = view(request)
            body = body.encode('utf-8')

        start_response(code, [('Content-Type', content_type)])
        return [body]

    @staticmethod
    def get_content_type(file_path, content_types_map=CONTENT_TYPES_MAP):
        file_name = path.basename(file_path).lower() # styles.css
        extension = path.splitext(file_name)[1] # .css
        print(extension)
        return content_types_map.get(extension, "text/html")

    @staticmethod
    def get_static(static_dir, file_path):
        path_to_file = path.join(static_dir, file_path)
        with open(path_to_file, 'rb') as f:
            file_content = f.read()
        status_code = '200 OK'
        return status_code, file_content

    @staticmethod
    def decode_value(data):
        new_data = {}
        for k, v in data.items():
            val = bytes(v.replace('%', '=').replace("+", " "), 'UTF-8')
            val_decode_str = quopri.decodestring(val).decode('UTF-8')
            new_data[k] = val_decode_str
        return new_data
